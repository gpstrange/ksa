/**
 * KS App
 * Everthing starts from the entrypoint
 */
import React, { Component } from 'react';
import { Text } from 'react-native';
// import { ActivityIndicator } from 'react-native';
// import { Provider, connect } from 'react-redux';
// import PropTypes from 'prop-types';
// import { PersistGate } from 'redux-persist/es/integration/react';
// import Navigator from '@navigation';
// import configureStore from '@store/configureStore';
// const { persistor, store, addListener } = configureStore();

export default class Entrypoint extends Component {
    render() {
        return (
            <Text>Lets start!!</Text>
        );
    }
}
